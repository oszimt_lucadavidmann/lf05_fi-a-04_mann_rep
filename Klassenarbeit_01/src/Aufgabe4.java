
public class Aufgabe4 {
	
	public static void main(String[] args) {
		int zahl =5;
		System.out.print(romanNumerals(zahl));
	}
	
	//hier soll Ihr Quelltext hin
	
	
	public static char romanNumerals(int numberToConvert) {
		
		switch (numberToConvert) {
		case  1:
			return 'I';
		case  5:
			return 'V';
		case  10:
			return 'X';
		case  50:
			return 'L';
		case  100:
			return 'C';
		case  500:
			return 'D';
		case  1000:
			return 'M';
		default :
			return '?';
	}
	}
}
