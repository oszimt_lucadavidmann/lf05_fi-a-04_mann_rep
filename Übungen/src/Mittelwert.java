import java.util.Scanner;

public class Mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte für x und y festlegen:
      // ===========================
     
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      System.out.printf("Der Mittelwert ist %.2f\n", calculateMittelwert());
      
   }

   private static double calculateMittelwert() {
    Scanner scanner = new Scanner(System.in);
    
    int anzahl = 0;
    double x = 0;
	 System.out.print("Anzahl:");  
    anzahl = scanner.nextInt();
    
    for (int i = 1; i <= anzahl; i++) {
    	System.out.print("Zahl:");
    	x += scanner.nextDouble();
    }
    scanner.close();
	   return x / anzahl;
   }
   
  
}