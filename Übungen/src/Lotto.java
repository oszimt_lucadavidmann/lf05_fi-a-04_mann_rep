
public class Lotto {

	public static void main(String[] args) {
		arrayPrinter(arrayFiller());
		System.out.println();
		arraySearcherFor12(arrayFiller());
		System.out.println();
		arraySearcherFor13(arrayFiller());
	}
	
	
	public static int[] arrayFiller() {
		
		int [] numbers = {
				3,
				7,
				12,
				18,
				37,
				42
		};
		return numbers;
	}
	
	
	public static void arrayPrinter(int [] numbers) {
		
		System.out.print("[ ");
		
		for (int i = 0; i < numbers.length; i++) {
			System.out.print(numbers[i] + " ");
		}
		System.out.println("]");
	}
	

	
	public static void arraySearcherFor12(int [] numbers) {
		int target = 12;
		boolean found = false;
		for(int i = 0; i <numbers.length; i++) {
			if(numbers[i] == target) {
				found = true;
				break;
			}
		}
		
		if(!found) {
			System.out.println("Die Zahl " + target + " ist nicht in der Ziehung enthalten.");
		}
		else {
			System.out.println("Die Zahl " + target + " ist in der Ziehung enthalten.");
		}
	}
	
	public static void arraySearcherFor13(int [] numbers) {
		int target = 13;
		boolean found = false;
		for(int i = 0; i <numbers.length; i++) {
			if(numbers[i] == target) {
				found = true;
				break;
			}
		}
		
		if(!found) {
			System.out.println("Die Zahl " + target + " ist nicht in der Ziehung enthalten.");
		}
		else {
			System.out.println("Die Zahl " + target + " ist in der Ziehung enthalten.");
		}
	}
}
