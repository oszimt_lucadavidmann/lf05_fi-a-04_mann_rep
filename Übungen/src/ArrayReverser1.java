import java.util.Arrays;

public class ArrayReverser1 {

	public static void main(String[] args) {
		int[] numbers = {9,17,3,2,13};
		
		System.out.println(Arrays.toString(numbers));
		arrayReversed(numbers);
		System.out.println(Arrays.toString(numbers));

	}

	private static void arrayReversed(int[] numbers) {
		
		if(numbers == null) {
			System.out.println("Array ist Leer");
			System.exit(0);
		}
		
		else {
			int placeholder = 0;
			for (int i = 0;i < numbers.length / 2; i++) {
				placeholder = numbers[i];
				numbers[i] = numbers[numbers.length -1 -i];
				numbers[numbers.length -1 - i] = placeholder;
			}
		}
	}

}
