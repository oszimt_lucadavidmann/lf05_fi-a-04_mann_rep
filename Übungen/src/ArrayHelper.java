
public class ArrayHelper {

	public static void main(String[] args) {
		int[] toConvert = {9,17,3,2,13 };

		System.out.println(convertArrayToString(toConvert));
	}

	public static String convertArrayToString(int[] numbers) {
		String result = "[ ";
		
		for ( int i = 0; i < numbers.length; i ++) {
			result += numbers[i] + " ";
		}
		result += "]";
		return result;
	}
	
}
