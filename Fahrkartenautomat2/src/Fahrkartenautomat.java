import java.util.Scanner;

class Fahrkartenautomat {

	public static double fahrkartenbestellungErfassen() {

		Scanner tastatur = new Scanner(System.in);

		String [] fahrkartenBezeichnung = {"Einzelfahrschein Berlin BC","Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC"
				, "Tageskarte Berlin ABC","Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"}; 
		
		double [] fahrkartenPreise = {2.90,3.30,3.60,1.90,8.60,9.00,9.60,23.50,24.30,24.90};
		
		
		System.out.println("Bitte Fahrkarte auswählen:");
		System.out.println("==========================");
		for (int i = 0; i < fahrkartenBezeichnung.length; i++) {
			System.out.println("[" + (i + 1) + "] " + fahrkartenBezeichnung[i]);
		}
		
		int fahrkartenNummer = tastatur.nextInt() - 1;
		
		
		
	//	double zuZahlenderBetrag;

		int anzahl;

	//	System.out.print("Zu zahlender Betrag (EURO): ");

	//	zuZahlenderBetrag = tastatur.nextDouble();

	//	if(zuZahlenderBetrag < 1) {
	//		zuZahlenderBetrag = 1;
	//		System.out.println("Die angegebene Preis ist ungültig! Fahre mit 1 € fort...");
	//	}
		
		System.out.print("Anzahl der Tickets: ");
		
		anzahl = tastatur.nextInt();
		
		
	if(anzahl <= 1 || anzahl > 10 ) {
		anzahl = 1;
		System.out.println("Die angegebene Anzahl ist ungültig! Fahre mit 1 fort...");
	}

		//zuZahlenderBetrag = zuZahlenderBetrag * anzahl;

		return fahrkartenPreise[fahrkartenNummer] * anzahl;

	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {

		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag = 0.0;

		while (eingezahlterGesamtbetrag < (zuZahlenderBetrag)) {


			// System.out.printf("%.2f", zuZahlenderBetrag - eingezahlterGesamtbetrag);

			System.out.printf("%.2f", zuZahlenderBetrag - eingezahlterGesamtbetrag);

			System.out.println(" Euro");

			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");

			double eingeworfeneMünze = tastatur.nextDouble();

			eingezahlterGesamtbetrag += eingeworfeneMünze;

		}

		return eingezahlterGesamtbetrag;

	}

	public static void fahrkartenAusgeben() {

		System.out.println("\nFahrschein wird ausgegeben");

		for (int i = 0; i < 8; i++) {

			System.out.print("=");

			try {

				Thread.sleep(250);

			} catch (InterruptedException e) {

				// TODO Auto-generated catch block

				e.printStackTrace();

			}

		}

		System.out.println("\n\n");

	}

	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {

		double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;

		if (rückgabebetrag > 0.0) {

			System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");

			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen

			{

				System.out.println("2 EURO");

				rückgabebetrag -= 2.0;

			}

			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen

			{

				System.out.println("1 EURO");

				rückgabebetrag -= 1.0;

			}

			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen

			{

				System.out.println("50 CENT");

				rückgabebetrag -= 0.5;

			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen

			{

				System.out.println("20 CENT");

				rückgabebetrag -= 0.2;

			}

			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen

			{

				System.out.println("10 CENT");

				rückgabebetrag -= 0.1;

			}

			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen

			{

				System.out.println("5 CENT");

				rückgabebetrag -= 0.05;

			}

		}
		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"

				+ "Wir wünschen Ihnen eine gute Fahrt.");
	}

	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);

		double zuZahlenderBetrag;

		double eingezahlterGesamtbetrag;


		zuZahlenderBetrag = fahrkartenbestellungErfassen();

		// Geldeinwurf

		// -----------

		eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);

		// Fahrscheinausgabe

		// -----------------

		fahrkartenAusgeben();

		// Rückgeldberechnung und -Ausgabe

		// -------------------------------

		rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);

		tastatur.close();

	}

}